Welcome to my application test for Marmalade.

To run the game, open the main.unity3d scene in Assets/Scenes

Some here are some considerations about this project:

- My aim was to create a modular architecture, dividing the application in the most possible number of modules that are independent.

- I tried to avoid almost all use of Unity's Update calls, therefore creating an architecture that starts with a parent object Gameflow,
that is the only object that calls the Unity Update method. From that object I propagate the updates to the rest of the components in the application.
This has two advantages: it gives more control to the developers of what happens when, and it actually is more performant that having all objects call Unity's
Update method.

- Related to the previous point, I also tried to replace the use of Monobehaviours with default classes, therefore reducing the number of gameobjects with scripts in them. Also this
allows for more flexibility when wanting to change the class of an object as they are not attached to a gameobject.

- Also, I went for a hierarchy structure where the objects with visual content are separated from objects with scripts, allowing for a more controlled scene environment.

- For the AI, I went with two approaches, reactive AI and FSM. The orange car uses Reactive approach, the other two use FSM.

- I implemented input support for pc and mobile, but mobile version is not optimized.

- Now a curiosity point, I avoided using foreach loops, as they create memory leaks in C#

Any questions please feel free to ask me.