﻿using UnityEngine;

public class MoneyItem  {
	private MoneyModule _moneyModule;
	private CollisionDetector _collisionDetector;
	private AnimationDetector _animationDetector;
	private Animator _animator;

	public GameObject playerCarObject{ get; private set; }

	public GameObject moneyObject{ get; private set;}
	public EMONEY moneyType{ get; private set;}

	public MoneyItem(MoneyModule m, EMONEY mType, GameObject gobj)
	{
		_moneyModule = m;
		moneyType = mType;
		moneyObject = gobj;
		_animator = moneyObject.GetComponent< Animator >();
		
		_collisionDetector = moneyObject.GetComponent< CollisionDetector >();
		_animationDetector = moneyObject.GetComponent< AnimationDetector >();
		_collisionDetector.OnTriggerEnterEvent += OnTriggerEnterHandler;
		_animationDetector.OnAnimEndEvent += OnAnimEndEventHandler;
	}
    ~MoneyItem()
	{
		_collisionDetector.OnTriggerEnterEvent -= OnTriggerEnterHandler;
		_animationDetector.OnAnimEndEvent -= OnAnimEndEventHandler;
	}
	
	void OnTriggerEnterHandler(Collider other)
	{
        //ensure that only one player picks up the money
		if(playerCarObject == null)
		{
			playerCarObject = other.gameObject;
			Gameflow.GetModule< AudioModule >().PlaySFX(ESFX.PICKUP_MONEY);
			PlayAnim("moneyPickup");
		}
	}

    private void OnAnimEndEventHandler(string anim)
    {
		switch(anim)
		{
			case "moneyPickup":
				_moneyModule.PickupMoney(this);
				playerCarObject = null;
			break;
			default:
			break;
		}
    }

    public void ToggleMoneyObj(bool v)
    {
        moneyObject.SetActive(v);
		if(v)
		{
			PlayAnim("moneyPopup");
		}
    }

	private void PlayAnim(string anim)
	{
		_animator.Play(anim);
	}

    public void SetMoneyObjPosition(Vector3 randPos)
    {
		moneyObject.transform.position = randPos;
    }

    public Vector3 GetMoneyPosition()
    {
		return moneyObject.transform.position;
    }
}
