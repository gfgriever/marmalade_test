﻿using System;
using UnityEngine;

public enum ECAR
{
	P1,
	P2,
	P3,
	P4,
	TRAFFIC,
	COUNT
}

public class CarEntity {
	protected enum ECARSTATE
	{
		IDLE,
		MOVING,
		COUNT
	}
	private ECARSTATE _carState;
	protected CarsModule _carPlayersModule;
	protected CollisionDetector _collisionDetector;
	public CarMovement carMovement{ get; private set;}
	public Transform carTransform{ get; private set;}
	public GameObject carGameObject{ get; private set; }
	public ECAR carType{ get; protected set;}
	public bool isHuman{get ;protected set;}

	public CarEntity(GameObject carObj, Vector3 arenaPosition)
	{
		isHuman = false;
		_carPlayersModule = Gameflow.GetModule< MinigameCarsModule >().carPlayersModule;
		carGameObject = carObj;
		carTransform = carGameObject.transform;
		ChangeCarState(ECARSTATE.IDLE);
		carMovement = new CarMovement(this, carTransform, arenaPosition);

		//using the _collisionDetector on the GameObject of the car we are able to catch unity's physics methods for collision checking
		_collisionDetector = carGameObject.GetComponent< CollisionDetector >();
		_collisionDetector.OnCollisionEnterEvent += OnCollisionEnterHandler;
		_collisionDetector.OnTriggerEnterEvent += OnTriggerEnterHandler;
	}

    public virtual void UpdateCar(float dt)
	{
		switch(_carState)
		{
			case ECARSTATE.MOVING:
				UpdateCarMoveState(dt);
			break;
			default:
			break;
		}
	}

	protected void UpdateCarMoveState(float dt)
	{
		carMovement.UpdateCarPosition(dt);
	}

	public void MoveCarTo(Vector3 waypoint, bool validateWaypoint = true)
	{
		_carState = ECARSTATE.MOVING;
		carMovement.SetNewWaypoint(waypoint, validateWaypoint);
	}

	protected void ChangeCarState(ECARSTATE state)
	{
		_carState = state;
	}


    protected virtual void OnTriggerEnterHandler(Collider other)
    {
		//method called when the carGameObject fires an OnTriggerEnter method
    }

    protected virtual void OnCollisionEnterHandler(Collision other)
    {
		//method called when the carGameObject fires an OnCollisionEnter method
    }

	public virtual void CarReachedWaypoint()
	{
		ChangeCarState(ECARSTATE.IDLE);
	}

    public void ToggleHeadLights(bool v)
    {
		carGameObject.GetComponentInChildren< Light >(true).enabled = v;
    }
}
