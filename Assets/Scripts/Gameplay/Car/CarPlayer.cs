﻿using UnityEngine;

public class CarPlayer : CarEntity
{
    public CarPlayer(ECAR pType, GameObject carObj, Vector3 arenaPosition) : base(carObj, arenaPosition)
    {
		carType = pType;
        isHuman = true;
    }
    
    protected override void OnTriggerEnterHandler(Collider other)
    {
        switch(other.tag)
        {
            case "Traffic":
                _carPlayersModule.CarHitTraffic(this);
            break;
            default:
            break;
        }
    }

    protected override void OnCollisionEnterHandler(Collision other)
    {
        Debug.Log("OnCollisionEnterHandler " + other.collider.tag);
    }
}
