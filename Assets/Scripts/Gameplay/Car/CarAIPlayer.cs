﻿using UnityEngine;

public class CarAIPlayer : CarEntity
{
	private BaseAI _ai;

    public CarAIPlayer(ECAR pType, GameObject carObj, Vector3 arenaPosition, BaseAI ai) : base(carObj, arenaPosition)
    {
		carType = pType;
		_ai = ai;
        _ai.SetCar(this, _carPlayersModule);
        ChangeCarState(ECARSTATE.MOVING);
    }

    public override void UpdateCar(float dt)
    {
        base.UpdateCar(dt);
        _ai.UpdateAI(dt);
    }

    public override void CarReachedWaypoint()
	{
		_ai.WaypointReached();
	}

    protected override void OnTriggerEnterHandler(Collider other)
    {
		switch(other.tag)
		{
			case "Traffic":
				_carPlayersModule.CarHitTraffic(this);
			break;
			default:
			break;
		}

        _ai.OnTriggerEnterHandler(other);
    }

    protected override void OnCollisionEnterHandler(Collision other)
    {
        _ai.OnCollisionEnterHandler(other);
    } 
}