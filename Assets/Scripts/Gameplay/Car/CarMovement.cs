﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement  {
	private float _carMoveMultiplier;
	private float _carRotateMultiplier;
	private Vector3 _arenaPosition;
	private Transform _carTransform;	
	private CarEntity _carEntity;
	private Quaternion _targetRotation;
	public Vector3 nextWaypoint{ get; private set; }

	public CarMovement(CarEntity ce, Transform carTrf, Vector3 arenaPosition)
	{
		_carEntity = ce;
		_carTransform = carTrf;
		_carMoveMultiplier = GameConfig.CAR_MOVE_MULTIPLIER;
		_carRotateMultiplier = GameConfig.CAR_ROTATE_MULTIPLIER;
		_arenaPosition = arenaPosition;
		nextWaypoint = _carTransform.position;
	}
	
	public void SetMoveMultiplier(float f)
	{
		_carMoveMultiplier = f;
	}

	public void SetRotateMultiplier(float f)
	{
		_carRotateMultiplier = f;
	}

	public virtual void UpdateCarPosition(Vector3 waypoint, float dt)
	{
		//trying to simulate the behaviour of a real car, the position is always updated by incrementing relative to the direction vector
		_targetRotation = GetTargetRotation(waypoint);
		_carTransform.rotation = Quaternion.Slerp(_carTransform.rotation, _targetRotation, _carRotateMultiplier * dt);
		_carTransform.position -= _carTransform.up * dt * _carMoveMultiplier;
	}

	private Quaternion GetTargetRotation(Vector3 waypoint)
	{
		Vector3 dir = (waypoint - _carTransform.position).normalized;
		Vector3 newRot = Quaternion.LookRotation(dir).eulerAngles;
		newRot.x = -90f;
		return Quaternion.Euler(newRot);
	}

	public void LookToWaypoint(Vector3 waypoint)
	{
		_targetRotation = GetTargetRotation(waypoint);
		_carTransform.rotation = _targetRotation;
	}

	public Vector3 GetValidWaypoint(Vector3 waypoint, float radius)
	{
		Vector3 wptoCenter = waypoint - _arenaPosition;
		Vector3 validWaypoint = Vector3.ClampMagnitude(wptoCenter, radius);
		validWaypoint.y = _carTransform.position.y;
		return validWaypoint;
	}

	public float GetDistanceFromCenter()
	{
		return Vector3.Distance(_carTransform.position, _arenaPosition);
	}

	public void SetCarPosition(Vector3 pos)
	{
		_carTransform.position = pos;
	}

	public void UpdateCarPosition(float dt)
	{
		if(!HasCarReachedWaypoint())
		{
			UpdateCarPosition(nextWaypoint, dt);
		}
		else
		{
			_carEntity.CarReachedWaypoint();
		}
	}

	protected bool HasCarReachedWaypoint()
	{
		float dist = Vector3.Distance(nextWaypoint, _carTransform.position); 
		return dist <= GameConfig.PLAYER_MIN_DISTANCE_MOVE;
	}

	public void SetNewWaypoint(Vector3 waypoint, bool validWaypoint)
	{
		nextWaypoint = waypoint;
		if(validWaypoint)
		{
			nextWaypoint = GetValidWaypoint(waypoint, GameConfig.ARENA_RADIUS);
		}
		_targetRotation = GetTargetRotation(nextWaypoint);
	}

	public Vector3 GetRandomArenaPoint(float radius, bool inCircumference)
	{
		Vector2 circleToUse = Random.insideUnitCircle;
		
		//by normalizing the circleToUse vector and then multiplying by the radius we get a point in the circumference
		if(inCircumference)
		{
			circleToUse.Normalize();
		}
		
		Vector2 circumferencePoint = circleToUse * radius;
		Vector3 waypoint = new Vector3(circumferencePoint.x, _carTransform.position.y, circumferencePoint.y);

		return waypoint;
	}

}
