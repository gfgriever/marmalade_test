﻿using System.Collections.Generic;
using UnityEngine;
public class CarTraffic : CarEntity {
	
	private List< Vector3 > _wayPoints;

	private int _currentWaypoint;
	private TrafficModule _trafficModule;

    public CarTraffic(TrafficModule trafficModule, GameObject carObj, Vector3 arenaPosition) : base(carObj, arenaPosition)
    {
		_trafficModule = trafficModule;
		carType = ECAR.TRAFFIC;
		_wayPoints = new List< Vector3 >();
		carMovement.SetMoveMultiplier(GameConfig.TRAFFIC_MOVE_MULTIPLIER);
		carMovement.SetRotateMultiplier(GameConfig.TRAFFIC_ROTATE_MULTIPLIER);
    }

	public override void UpdateCar(float dt)
	{
		base.UpdateCar(dt);
	}

	public void StartTrafficCar()
	{
		carGameObject.SetActive(true);
		CalculateNewWaypoints();
		SetStartPosition();
		GoToNextWaypoint();
	}

	private void SetStartPosition()
	{
		Vector3 pos = CreateRandomWaypoint(GameConfig.TRAFFIC_ARENA_RADIUS_IN, true);
		carMovement.SetCarPosition(pos);
		carMovement.LookToWaypoint(_wayPoints[0]);
	}

	private void CalculateNewWaypoints()
	{
		_currentWaypoint = -1;
		_wayPoints.Clear();
		int numWaypoints = GetRandNumWaypoints();
		Vector3 waypoint;
		for(int i = 0 ; i < numWaypoints - 1; ++i)
		{
			waypoint = CreateRandomWaypoint(GameConfig.ARENA_RADIUS, false);
			_wayPoints.Add(waypoint);
		}
		_wayPoints.Add(CreateRandomWaypoint(GameConfig.TRAFFIC_ARENA_RADIUS_OUT, true));
	}

	private void GoToNextWaypoint()
	{
		++_currentWaypoint;
		if(_currentWaypoint >= _wayPoints.Count)
		{
			//hide the traffic car
			carGameObject.SetActive(false);
			_trafficModule.HideTrafficCar(this);
		}
		else
		{
			//with traffic we don't want to validate waypoints as the traffic cars can move out of the arena
			MoveCarTo(_wayPoints[_currentWaypoint], false);
		}
	}

	public override void CarReachedWaypoint()
	{
		GoToNextWaypoint();
	}

	private int GetRandNumWaypoints()
	{
		return Random.Range(GameConfig.TRAFFIC_MIN_WAYPOINTS, GameConfig.TRAFFIC_MAX_WAYPOINTS + 1);
	}

	//returns a point inside the arena or in a circumference larger than the arena, where the traffic cars spawn and despawn
	private Vector3 CreateRandomWaypoint(float radius, bool inCircumference)
	{
		return carMovement.GetRandomArenaPoint(radius, inCircumference);
	}

	 protected override void OnTriggerEnterHandler(Collider other)
    {
		if(other.tag != "Traffic" && other.tag != "Money")
		{
			Gameflow.GetModule< AudioModule >().PlaySFX(ESFX.HORN);
		}
    }
}
