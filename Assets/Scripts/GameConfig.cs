﻿public static class GameConfig{

	/*
	
		this class can be adapted to read/write a json/xml file, facilitating configurations loaded
		in runtime and consequently easing the use of A/B testing
	
	 */


	//arena radius where the cars are allowed to move
	public const float ARENA_RADIUS = 35f;

	//countdown at the beginning of the game
   	public const float PRESTART_COUNTDOWN = 3f;
	
	//threshold used to stop a car from moving
	public const float PLAYER_MIN_DISTANCE_MOVE = 3f;

	//multipliers for the rotation and translation of the car
	public const float CAR_ROTATE_MULTIPLIER = 5f;
	public const float CAR_MOVE_MULTIPLIER = 15f;

	//this radius is used to determine the area where money objects can spawn
	public const float MONEY_SPAWN_MAX_RADIUS = 20f;

	//these constants reflect the size of the money objects pool
	public const int MONEY_POOL_1K = 1;//7;
	public const int MONEY_POOL_5K = 1;//5;
	public const int MONEY_POOL_10K = 1;//2;
	public const int MONEY_POOL_50K = 1;

	//minimum and maximum time without a money object appearing
	public const float MONEY_MIN_COOLDOWN = 2f;
	public const float MONEY_MAX_COOLDOWN = 3f;

	//duration of the match in seconds
	public const float MATCH_DURATION_SECONDS = 30f;

	//traffic cars pool size
	public const int TRAFFIC_POOL_SIZE = 3;

	//minimum and maximum time without a traffic car appearing
	public const float TRAFFIC_MIN_COOLDOWN = 3f;
	public const float TRAFFIC_MAX_COOLDOWN = 5f;

	//min and max waypoints a traffic car can pass
	public const int  TRAFFIC_MIN_WAYPOINTS = 2;
	public const int  TRAFFIC_MAX_WAYPOINTS = 4;

	//radius used to calculate start and finish waypoints for the traffic
	public const float TRAFFIC_ARENA_RADIUS_IN = ARENA_RADIUS + 15f;
	public const float TRAFFIC_ARENA_RADIUS_OUT = ARENA_RADIUS + 15f;

	//traffic cars move and rotate multipliers
	public const float TRAFFIC_ROTATE_MULTIPLIER = 2f;
	public const float TRAFFIC_MOVE_MULTIPLIER = 9f;

	//Reactive AI raycast angle variance
	public const float REACTIVE_AI_RAYCAST_ANGLE_VARIANCE = 25f;

	//Reactive AI raycast check period in seconds
	public const float REACTIVE_AI_CHECK_PERIOD = 0.01f;

	//Reactive AI raycast distance
	public const float REACTIVE_AI_RAYCAST_DIST = ARENA_RADIUS / 2f;

	//FSM AI dangerous radius considered when deciding whether to choose a new waypoint to avoid collision
	public const float FSM_AI_DANGEROUS_RADIUS = 15f;


	//scores addition for money and traffic collision
	public const int SCORE_MONEY1K = 1;	
	public const int SCORE_MONEY5K = 5;
	public const int SCORE_MONEY10K = 10;
	public const int SCORE_MONEY50K = 50;
	public const int SCORE_TRAFFIC = -10;

	//how long until it gets dark, the directional light intensity when it's dark, wgen to turn on headlights
	public const float DAY_NIGHT_FRACTION = 7.5f/10f;
	public const float DAY_NIGHT_MIN_LIGHT_INTENSITY = 0.25f;

	public const float DAY_NIGHT_ENABLE_HEADLIGHTS = 6/10f;

	//audio considerations, such as volume and pool size of the audiosource
	public const float AUDIO_MUSIC_VOLUME = 0f;
	public const float AUDIO_SFX_VOLUME = 0f;
	public const int AUDIO_DEFAULT_AUDIOSOURCES = 3;
    public const int AUDIO_MAX_SFX_AUDIOSOURCES = 5;
}
