﻿using System.Collections.Generic;
using UnityEngine;

public class InputModule : BaseModule
{
	private List< InputController > _controllers;

    public override void InitModule()
    {
		_controllers = new List< InputController >();
    }

    public override void UpdateModule(float dt)
    {
			for(int i = 0 ; i < _controllers.Count; ++i)
			{
				_controllers[i].UpdateController(dt);
			}
    }

	public void AddController(InputController ic)
	{
		if(!_controllers.Contains(ic))
		{
			_controllers.Add(ic);
		}
	}

	public void RemoveController(InputController ic)
	{
		_controllers.Remove(ic);
	}

    public void AssignController(CarPlayer p)
    {
		switch(p.carType)
		{
			case ECAR.P1: //assign mouse and touch controller to player 1
				if(Input.touchSupported)
				{
					AddController(new FingerController(p));
				}
				else
				{
					AddController(new MouseCarController(p));
				}
				
			break;
			default:
			break;
		}
    }
}