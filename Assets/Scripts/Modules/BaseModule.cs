﻿using UnityEngine;
using System.Collections.Generic;

public abstract class BaseModule : MonoBehaviour {

	public virtual void InitModule(){}
	public virtual void UpdateModule(float dt){}

	public virtual void DestroyModule(){}

}
