﻿using System.Collections.Generic;
using UnityEngine;

public class ScoreModule : BaseModule
{
	public delegate void ScoreUpdateDelegate(ECAR player, int value);
	public event ScoreUpdateDelegate ScoreUpdateEvent;

	Dictionary< CarEntity, int > _scores;


    public override void InitModule()
    {
		_scores = new Dictionary< CarEntity, int >();
    }

	public void ResetScores(List< CarEntity > players)
	{
		_scores.Clear();
		for(int i = 0 ; i < players.Count; ++i)
		{
			_scores.Add(players[i], 0);
		}
	}

	private void AddMoneyScore(CarEntity player, EMONEY money)
	{
		ScoreUpdate(player, GetMoneyValue(money));
	}

	private void AddScoreTraffic(CarEntity player)
	{
		ScoreUpdate(player, GameConfig.SCORE_TRAFFIC);
	}

	private void ScoreUpdate(CarEntity p, int value)
	{
		//safeguard to prevent score below 0
		_scores[p] = Mathf.Clamp(_scores[p] + value,0, int.MaxValue);

		if(ScoreUpdateEvent != null)
		{
			ScoreUpdateEvent(p.carType, _scores[p]); //this event will be used to update the UI about the score
		}
	}
	private int GetMoneyValue(EMONEY money)
	{
		switch(money)
		{
			case EMONEY.ONE:
				return GameConfig.SCORE_MONEY1K;
			case EMONEY.FIVE:
				return GameConfig.SCORE_MONEY5K;
			case EMONEY.TEN:
				return GameConfig.SCORE_MONEY10K;
			case EMONEY.FIFTY:
				return GameConfig.SCORE_MONEY50K;
		}
		//error case
		return 0;
	}

    public void CarHitTrafficHandler(CarEntity car)
	{
		AddScoreTraffic(car);
	}

    public void MoneyPickedHandler(MoneyItem money)
    {
		CarEntity car = GetCarFromCollider(money.playerCarObject);
		AddMoneyScore(car, money.moneyType);
    }

	private CarEntity GetCarFromCollider(GameObject playerCarObj)
	{
		foreach(KeyValuePair< CarEntity, int > kv in _scores)
		{
			if(kv.Key.carGameObject == playerCarObj)
			{
				return kv.Key;
			}
		}
		return null;
	}

	public CarEntity GetWinner()
	{
		int score = 0;
		CarEntity winner = null;
		foreach(KeyValuePair< CarEntity, int > kv in _scores)
		{
			if(kv.Value > score)
			{
				winner = kv.Key;
				score = kv.Value;
			}
		}
		return winner;
	}
}
