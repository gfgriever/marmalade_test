﻿using UnityEngine.UI;

public class TimeModule : BaseModule {

	private Text _timerText;
	private float _timeRemainingSeconds;
	
	private bool _timerActive;
	private float _sustainMsgTime;

	public delegate void TimeEndDelegate();

	public event TimeEndDelegate TimeEndEvent;

	public override void InitModule()
	{
		ResetTimer();
		StopTimer();
	}

	public override void UpdateModule(float dt)
	{
		if(_timerActive)
		{
			_timeRemainingSeconds -= dt;
			_sustainMsgTime -= dt;
			if(_timeRemainingSeconds <= 0)
			{
				StopTimer();
				if(TimeEndEvent != null)
				{
					TimeEndEvent();
				}
			}
			if(_sustainMsgTime < 0) //this is used only in a specific case, where we want the initial GO! message to keep on screen for a while
			{
				FormatTimer();
			}
		}
	}

	private void FormatTimer()
	{
		int time = (int)_timeRemainingSeconds;
		_timerText.text = time.ToString();
	}

	public void StartTimer()
	{
		_timerActive = true;
	}

	public void ResetTimer()
	{
		_timeRemainingSeconds = GameConfig.MATCH_DURATION_SECONDS;
	}

	public void StopTimer()
	{
		_timerActive = false;
	}

    public void SetTimerText(Text timerText)
    {
		_timerText = timerText;
    }

    public void SetTimerMessage(string v, float sustainTime = -1f)
    {
		_sustainMsgTime = sustainTime;
		_timerText.text = v;
    }
}
