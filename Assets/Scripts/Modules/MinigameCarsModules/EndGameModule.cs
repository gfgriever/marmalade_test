﻿using UnityEngine;

public class EndGameModule : BaseModule {

	CarEntity _winner;

	public void MatchEnd(CarEntity winner)
	{	
		if(winner != null)
		{
			_winner = winner;
		}
	}
    public override void UpdateModule(float dt)
	{
		if(_winner != null)
		{
			Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, _winner.carTransform.position + new Vector3(0f,30f,0f), dt);
			Camera.main.transform.LookAt(_winner.carTransform);
		}
	}
}
