﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class CarsModule : BaseModule
{
	private Vector3 _arenaPosition;

	[SerializeField]
	private GameObject[] _carPrefabs;

	[SerializeField]
	private Transform[] _spawnPoints;

	[SerializeField]
	private Transform _carParentTrf;

	public List< CarEntity > cars{ get; private set;}

	public delegate void CarHitTrafficDelegate(CarEntity car);
	public event CarHitTrafficDelegate CarHitTrafficEvent;

    public override void InitModule()
    {
        cars = new List< CarEntity >();
    }

    public override void UpdateModule(float dt)
    {
        for(int i = 0 ; i < cars.Count; ++i)
        {
            cars[i].UpdateCar(dt);
        }
    }

	public void AssignControllers()
	{
		CarPlayer p1 = GetHumanPlayer();
		Gameflow.GetModule< InputModule >().AssignController(p1);
	}

	public void SpawnPlayer(ECAR pType)
	{
		GameObject carObj = SpawnCarObject(pType);
		CarPlayer p = new CarPlayer(pType, carObj, _arenaPosition);
		cars.Add(p);
	}

    public void SpawnAI(ECAR pType, BaseAI ai)
    {
		GameObject carObj = SpawnCarObject(pType);
		CarAIPlayer p = new CarAIPlayer(pType, carObj, _arenaPosition, ai);
		cars.Add(p); 
    }

	private GameObject SpawnCarObject(ECAR pType) //spawn the gameobject car of a player/AI
	{
		int playerIdx = (int) pType;
		Transform spawnTransform = _spawnPoints[playerIdx];
		GameObject carObj = Instantiate(_carPrefabs[playerIdx], spawnTransform.position, spawnTransform.rotation);
		carObj.transform.SetParent(_carParentTrf);
		return carObj;
	}

    public void SetArenaPosition(Vector3 arenaPosition)
    {
		_arenaPosition = arenaPosition;
    }

    public CarEntity GetCarPlayer(GameObject gameObject)
    {
		for(int i = 0 ; i < cars.Count; ++i)
		{
			if(cars[i].carGameObject == gameObject)
			{
				return cars[i];
			}
		}
		return null;
    }
	public CarEntity GetCarEntity(ECAR c)
    {
		for(int i = 0 ; i < cars.Count; ++i)
		{
			if(cars[i].carType == c)
			{
				return cars[i];
			}
		}
		return null;
    }

	public CarPlayer GetHumanPlayer()
    {
		for(int i = 0 ; i < cars.Count; ++i)
		{
			if(cars[i].isHuman)
			{
				return (CarPlayer)cars[i];
			}
		}
		return null;
    }

    public void CarHitTraffic(CarEntity carEntity)
    {
		if(CarHitTrafficEvent != null)
		{
			CarHitTrafficEvent(carEntity);
		}
    }

    public void TurnOnCarHeadLights()
    {
		for(int i = 0 ; i < cars.Count; ++i)
		{
			cars[i].ToggleHeadLights(true);
		}
    }
}
