﻿using System;
using UnityEngine;

public class ArenaModule : BaseModule
{
	[SerializeField]
	Transform _arenaTransform;


    //This prevents the player from going to outside the arena

    public Vector3 GetArenaWorldPosition()
	{
		return _arenaTransform.position;
	}

    public override void InitModule()
    {
    }
}
