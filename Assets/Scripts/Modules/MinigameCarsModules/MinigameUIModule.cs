﻿using UnityEngine;
using UnityEngine.UI;

public class MinigameUIModule : BaseModule
{
	[SerializeField]
	Text _timerText;

	[SerializeField]
	Text[] _scoreUIsArray;

	[SerializeField]
	Text _winnerText;

	public Text timerText{ get{ return _timerText; }}

    public override void InitModule()
    {
		ResetScores();
    }

	private void ResetScores()
	{
		for(int i = 0 ; i < _scoreUIsArray.Length; ++i)
		{
			_scoreUIsArray[i].text = "$0";
		}
	}


    public void UpdateScoreHandler(ECAR car, int value)
    {
        _scoreUIsArray[(int)car].text = "$" + value.ToString();
    }

    public void MatchEnd(CarEntity winner)
    {	
		_winnerText.enabled = true;
		if(winner != null)
		{
			if(winner.carType == ECAR.P1)
			{
				_winnerText.text = "YOU WIN!";
			}
			else
			{
				_winnerText.text = winner.carType + " WINS!";
			}
		}
		else
		{
			_winnerText.text = "DRAW!";
		}
	}
}
