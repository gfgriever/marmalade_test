﻿using UnityEngine;

public class DayNightCycleModule : BaseModule {
	
	[SerializeField]
	Transform _directionalLight;

	[SerializeField]
	bool _toggleDayNightCycle = false;

	private float _originalLightIntensity;
	private float _currentIntensity;
	private float _timeUntilFullDark;
	private Light _dirLight;
	private float _timeCounterUntilDark;

	private bool _enableHeadlights;

	public delegate void NightFallDelegate();
	public event NightFallDelegate OnNightFallEvent;

	public override void InitModule()
	{
		_dirLight = _directionalLight.GetComponent< Light >();
		ResetDayNight();
	}


	private void ResetDayNight()
	{
		_enableHeadlights = false;
		_originalLightIntensity = _dirLight.intensity;
		_timeCounterUntilDark = 0;
		_timeUntilFullDark = GameConfig.MATCH_DURATION_SECONDS * GameConfig.DAY_NIGHT_FRACTION;
	}

	public override void UpdateModule(float dt)
	{
		if(_toggleDayNightCycle)
		{
			//_timeCounter runs between 0 and 1, where 1 is equivalent to _timeUntilFullDark
			if( _timeCounterUntilDark <= 1f)
			{
				_currentIntensity = Mathf.Lerp(_originalLightIntensity, GameConfig.DAY_NIGHT_MIN_LIGHT_INTENSITY, _timeCounterUntilDark);
				_dirLight.intensity = _currentIntensity; 

				//this allows us to adapt the lerp to be executed for a given duration, in this case _timeUntilFullDark
				_timeCounterUntilDark += dt /_timeUntilFullDark;

				//event to signal night has fallen
				if(!_enableHeadlights && _timeCounterUntilDark >= GameConfig.DAY_NIGHT_ENABLE_HEADLIGHTS)
				{
					_enableHeadlights = true;
					if(OnNightFallEvent != null)
					{
						OnNightFallEvent();
					}
				}
			}
		}
	}
}
