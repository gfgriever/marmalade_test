﻿using UnityEngine;

public enum EMONEY
{
    ONE,
    FIVE,
    TEN,
    FIFTY,
    COUNT
}
public class MoneyModule : BaseModule
{
    private ObjectPool< MoneyItem > _moneyPool;
    private float _nextMoneySpawn;
    private float _timeElapsed;
    private Vector3 _arenaPosition;
	
    [SerializeField]
	private Transform _moneyParentTrf;

    [SerializeField]
    private GameObject[] _moneyPrefabs;

    public delegate void MoneyPickedDelegate(MoneyItem money);
    public event MoneyPickedDelegate OnMoneyPickedEvent;

    public override void InitModule()
    {
        _moneyPool = new ObjectPool< MoneyItem >();
        _timeElapsed = 0f;
        SetNextMoneySpawn();
        AddToMoneyPool(EMONEY.ONE, GameConfig.MONEY_POOL_1K);
        AddToMoneyPool(EMONEY.FIVE, GameConfig.MONEY_POOL_5K);
        AddToMoneyPool(EMONEY.TEN, GameConfig.MONEY_POOL_10K);
        AddToMoneyPool(EMONEY.FIFTY, GameConfig.MONEY_POOL_50K);
    }

    public void SetArenaPosition(Vector3 arenaPosition)
    {
        _arenaPosition = arenaPosition;
    }

    public override void DestroyModule()
    {
        _moneyPool.DestroyPool();
    }

    public override void UpdateModule(float dt)
    {
        _timeElapsed += dt;

        UpdateMoneySpawn();
    }

    private void UpdateMoneySpawn()
    {
        if(_timeElapsed >= _nextMoneySpawn)
        {
            ShowMoney();
            SetNextMoneySpawn();
        }
    }

    private void SetNextMoneySpawn()
    {
        float rand = Random.Range(GameConfig.MONEY_MIN_COOLDOWN, GameConfig.MONEY_MAX_COOLDOWN);
        _nextMoneySpawn = _timeElapsed + rand;
    }


    //add a new money GameObject to the object pool
    private void AddToMoneyPool(EMONEY mType, int poolSize)
    {
        MoneyItem mi = null;
        GameObject prefab = _moneyPrefabs[(int)mType];
        Vector3 prefabPos = prefab.transform.position;
        Quaternion prefabRot = prefab.transform.rotation;   
        GameObject moneyObj;
        for(int i = 0 ; i < poolSize; ++i)
        {
            moneyObj = Instantiate(prefab, prefabPos, prefabRot);
            moneyObj.transform.SetParent(_moneyParentTrf);
            mi = new MoneyItem(this, mType, moneyObj);
            mi.ToggleMoneyObj(false);
            _moneyPool.AddObjectToPool(mi);
        }
    }


    private MoneyItem GetUnusedMoney()
    {
        MoneyItem mi;
        _moneyPool.GetUnusedObject(out mi);
        return mi;
    }

    public void ShowMoney()
    {
        MoneyItem mi = GetUnusedMoney();
        if(mi != null)
        {
            Vector3 randPos = GetRandPointInCircle(mi);
            mi.SetMoneyObjPosition(randPos);
            mi.ToggleMoneyObj(true);
        }
    }

    private Vector3 GetRandPointInCircle(MoneyItem mi)
	{
		Vector2 circleToUse = Random.insideUnitCircle;
		Vector2 circumferencePoint = circleToUse * GameConfig.ARENA_RADIUS;
		Vector3 waypoint = new Vector3(circumferencePoint.x, mi.GetMoneyPosition().y, circumferencePoint.y);
        waypoint += new Vector3(_arenaPosition.x, 0f, _arenaPosition.z);
		return waypoint;
	}

    public void PickupMoney(MoneyItem moneyItem)
    {
        //return moneyItem to the available money pool
       _moneyPool.FreeObject(moneyItem);

        //Score module should pickup this event
        if(OnMoneyPickedEvent != null)
        {
            OnMoneyPickedEvent(moneyItem);
        }
    }

    public MoneyItem GetClosestMoney(GameObject carGameObject)
    {
        float minDistance = float.MaxValue, currDist = 0f;
        MoneyItem mi = null, moneytoReturn = null;

        Vector3 carPos = carGameObject.transform.position;
        
        //from the pool of money objects in use, choose the closest to the carGameObject
        for(int i = 0 ; i < _moneyPool.objectsInUse.Count; ++i)
        {
            mi = _moneyPool.objectsInUse[i];
            currDist = Vector3.Distance(carPos, mi.GetMoneyPosition());
            
            if( currDist < minDistance)
            {
                moneytoReturn = mi;
                minDistance = currDist;
            }
        }
        return moneytoReturn;
    }
}
