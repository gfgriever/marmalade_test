﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TrafficModule : BaseModule {

	public List< CarTraffic > trafficCars{ get; private set;}
	public Vector3 arenaPosition{ get; private set;}

	[SerializeField]
	private GameObject _carTrafficPrefab;

	[SerializeField]
	private Transform _trafficParentTrf;

	private ObjectPool< CarTraffic > _trafficPool;
	private float _timeElapsed, _nextTrafficTime;
	private bool _nightMode;

    public override void InitModule()
	{
		_timeElapsed = 0f;
		_nightMode = false;
		_trafficPool = new ObjectPool< CarTraffic >();
		trafficCars = new List< CarTraffic >();
		InitTrafficCars();
		SetNextTrafficTime();
	}

	private void InitTrafficCars()
	{
		GameObject trafficObj;
		CarTraffic ct;
		for(int i = 0; i < GameConfig.TRAFFIC_POOL_SIZE; ++i)
		{
			trafficObj = Instantiate(_carTrafficPrefab, _carTrafficPrefab.transform.position, _carTrafficPrefab.transform.rotation);
			trafficObj.transform.SetParent(_trafficParentTrf);
			trafficObj.SetActive(false);
			ct = new CarTraffic(this, trafficObj, arenaPosition);
			_trafficPool.AddObjectToPool(ct);
		}
	}

    public void SetArenaPosition(Vector3 aPosition)
    {
		arenaPosition = aPosition;
    }

    public override void UpdateModule(float dt)
    {
        _timeElapsed += dt;
		UpdateTrafficSpawn();
		UpdateTrafficCars(dt);
    }

	private void UpdateTrafficSpawn()
	{
		//check if enough time has passed to spawn another traffic car
		if(CanShowTrafficCar())
		{
			ShowTrafficCar();
			SetNextTrafficTime(); //set the timer to spawn another traffic car
		}
	}

	private void UpdateTrafficCars(float dt)
	{
		for(int i = 0 ; i < trafficCars.Count; ++i)
		{
			trafficCars[i].UpdateCar(dt);
		}
	}

	private bool CanShowTrafficCar()
	{
		return _timeElapsed > _nextTrafficTime && trafficCars.Count < GameConfig.TRAFFIC_POOL_SIZE;	
	}

    public override void DestroyModule()
    {
		trafficCars = null;
		_trafficPool.DestroyPool();
    }

	public void HideTrafficCar(CarTraffic car)
	{
		trafficCars.Remove(car);
		_trafficPool.FreeObject(car);
	}

	private void ShowTrafficCar()
	{
		CarTraffic car;
		_trafficPool.GetUnusedObject(out car);
		trafficCars.Add(car);
		car.StartTrafficCar();
		car.ToggleHeadLights(_nightMode);
	}    
	private void SetNextTrafficTime()
    {
        float rand = UnityEngine.Random.Range(GameConfig.TRAFFIC_MIN_COOLDOWN, GameConfig.TRAFFIC_MAX_COOLDOWN);
        _nextTrafficTime = _timeElapsed + rand;
    }

	public void TurnOnCarHeadLights()
    {
		_nightMode = true;
		for(int i = 0 ; i < trafficCars.Count; ++i)
		{
			trafficCars[i].ToggleHeadLights(true);
		}
    }

}
