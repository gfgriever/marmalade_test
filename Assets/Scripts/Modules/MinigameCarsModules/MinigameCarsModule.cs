﻿using UnityEngine;
using System.Collections.Generic;

public class MinigameCarsModule : BaseModule
{
    private enum EMINIGAMESTATE
    {
        PRESTART,
        RUN,
        END,
        COUNT
    }
    private  ArenaModule _arenaModule;
    private TimeModule _timeModule;
    private ScoreModule _scoreModule;
    private MinigameUIModule _uiModule;
    private DayNightCycleModule _dayCycleModule;
    private EndGameModule _endGameModule;
    private float _timeCountdown;
    public TrafficModule trafficModule{ get; private set;}
	public CarsModule carPlayersModule{ get; private set;}
    public MoneyModule moneyModule{ get; private set;}
    private List< BaseModule > _modules;
    private EMINIGAMESTATE _minigameState;

    public override void InitModule()
    {
        InitSubModules();
        Vector3 arenaPosition = _arenaModule.GetArenaWorldPosition();
        carPlayersModule.SetArenaPosition(arenaPosition);
        trafficModule.SetArenaPosition(arenaPosition);
        moneyModule.SetArenaPosition(arenaPosition);
        
        SubscribeEvents();
        
        ChangeMinigameState(EMINIGAMESTATE.PRESTART);
    }

    private void InitSubModules()
    {
        _modules = new List< BaseModule >();
        _timeModule = InitModule< TimeModule >();
        moneyModule = InitModule< MoneyModule >();
        _arenaModule = InitModule< ArenaModule >();
        trafficModule = InitModule< TrafficModule >();
        _scoreModule = InitModule< ScoreModule >();
        _uiModule = InitModule< MinigameUIModule >();
        _dayCycleModule = InitModule< DayNightCycleModule >();
        _endGameModule = InitModule< EndGameModule >();
        carPlayersModule = InitModule< CarsModule >();

        //this should be done another way, don't really like it
        _timeModule.SetTimerText(_uiModule.timerText);
    }

	private T InitModule< T >() where T : BaseModule
	{
		T moduleVar = gameObject.GetComponentInChildren< T > ();
		_modules.Add(moduleVar);
		moduleVar.InitModule();
		return moduleVar;
	}

    public override void UpdateModule(float dt)
    {
        switch(_minigameState)
        {
            case EMINIGAMESTATE.PRESTART:
                UpdatePreMatch(dt);
            break;
            case EMINIGAMESTATE.RUN:
                UpdateAllSubmodules(dt);
            break;
            case EMINIGAMESTATE.END:
                _endGameModule.UpdateModule(dt);
            break;
            default:
            break;
        }
    }

    public override void DestroyModule()
    {
        UnsubscribeEvents();
    }

    private void UpdateAllSubmodules(float dt)
    {
		for(int i = 0 ; i < _modules.Count; ++i)
		{
			_modules[i].UpdateModule(dt);
		}
    }

    private void ChangeMinigameState(EMINIGAMESTATE state)
    {
        _minigameState = state;
        switch(state)
        {
            case EMINIGAMESTATE.PRESTART:
                Gameflow.GetModule< AudioModule >().PlayMusic(EMUSIC.CAR_MINIGAME);
                SpawnCars();
                PreMatchCountdown();
            break;
            case EMINIGAMESTATE.RUN:
                StartMatch();
                break;
            case EMINIGAMESTATE.END:
            //SHOW ENDSCREEN
            break;
            default:
            break;
        }
    }

    private void UpdatePreMatch(float dt)
    {
        _timeCountdown -= dt;
        if(_timeCountdown <= 0)
        {
            _timeModule.SetTimerMessage("GO!", 1f);
            ChangeMinigameState(EMINIGAMESTATE.RUN);
        }
        else
        {
            float ceil = Mathf.Ceil(_timeCountdown);
            _timeModule.SetTimerMessage(ceil.ToString());
        }
    }


    private void PreMatchCountdown()
    {
        _timeCountdown = GameConfig.PRESTART_COUNTDOWN;
    }


    private void SubscribeEvents()
    {
        //event to signal match end
        _timeModule.TimeEndEvent += MatchTimeEnd;

        //update score when money is picked
        moneyModule.OnMoneyPickedEvent += _scoreModule.MoneyPickedHandler;

        //event to update score when a car hits the traffic
        carPlayersModule.CarHitTrafficEvent += _scoreModule.CarHitTrafficHandler;

        //event to signal cars to turn on headlights when night falls
        _dayCycleModule.OnNightFallEvent += carPlayersModule.TurnOnCarHeadLights;
        _dayCycleModule.OnNightFallEvent += trafficModule.TurnOnCarHeadLights;

        //event to update ui about the score
        _scoreModule.ScoreUpdateEvent += _uiModule.UpdateScoreHandler;

    }

    private void UnsubscribeEvents()
    {
        _timeModule.TimeEndEvent -= MatchTimeEnd;
        moneyModule.OnMoneyPickedEvent -= _scoreModule.MoneyPickedHandler;
        carPlayersModule.CarHitTrafficEvent -= _scoreModule.CarHitTrafficHandler;
        _dayCycleModule.OnNightFallEvent -= carPlayersModule.TurnOnCarHeadLights;
        _dayCycleModule.OnNightFallEvent -= trafficModule.TurnOnCarHeadLights;
        _scoreModule.ScoreUpdateEvent -= _uiModule.UpdateScoreHandler;
    }

    private void SpawnCars()
    {
        carPlayersModule.SpawnPlayer(ECAR.P1);
        carPlayersModule.SpawnAI(ECAR.P2, new FSMAI());
        carPlayersModule.SpawnAI(ECAR.P3, new ReactiveAI());
        carPlayersModule.SpawnAI(ECAR.P4, new FSMAI());
    }

    public void StartMatch()
	{
        carPlayersModule.AssignControllers();
        _timeModule.StartTimer();
        _scoreModule.ResetScores(carPlayersModule.cars);
	}

    private void MatchTimeEnd()
    {
        Gameflow.GetModule< AudioModule >().StopMusic();
        CarEntity winner = _scoreModule.GetWinner();
        _uiModule.MatchEnd(winner);
        _endGameModule.MatchEnd(winner);
        ChangeMinigameState(EMINIGAMESTATE.END);
    }
}
