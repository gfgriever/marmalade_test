﻿using UnityEngine;
using System.Collections.Generic;
using System;

public enum ESFX
{
	UNKNOWN, //this key is used to signal a missing sfx in the list
	HORN,
	PICKUP_MONEY,
	START_GAME,
	END_GAME,
	COUNT
}

public enum EMUSIC
{
	UNKNOWN, //this key is used to signal a missing music in the list
	CAR_MINIGAME,
	COUNT
}

public class AudioModule : BaseModule {
	[SerializeField]
	private List< SAudioSFX > _sfxList;

	[SerializeField]
	private List< SAudioMusic > _musicList;
	private AudioSource _musicAudioSource;

	private SAudioMusic _currMusic;

	private ObjectPool< AudioSource > _sfxAudioSourcePool;
	

	public override void InitModule()
	{	
		_musicAudioSource = gameObject.AddComponent< AudioSource >();
		_musicAudioSource.playOnAwake = false;
		_musicAudioSource.volume = GameConfig.AUDIO_MUSIC_VOLUME;
		CreateSFXAudioSources();
	}

	public override void UpdateModule(float dt)
	{
		if(_sfxAudioSourcePool.objectsInUse.Count > 0)
		{
			TryFreeAudioSources(); //unfortunately there is no real good way in unity to find if an audiosource has stopped playing...
		}
	}

	private void TryFreeAudioSources()
	{
		List< AudioSource > toFree = new List< AudioSource >();
		for(int i = 0; i < _sfxAudioSourcePool.objectsInUse.Count; ++i)
		{
			if(!_sfxAudioSourcePool.objectsInUse[i].isPlaying)
			{
				toFree.Add(_sfxAudioSourcePool.objectsInUse[i]);
			}
		}
		_sfxAudioSourcePool.FreeObjects(toFree);
	}

    private void CreateSFXAudioSources()
    {
		_sfxAudioSourcePool = new ObjectPool< AudioSource >();
		for(int i = 0 ; i < GameConfig.AUDIO_DEFAULT_AUDIOSOURCES ; ++i)
		{
			CreateAudioSource();
		}
    }

	private AudioSource CreateAudioSource()
	{
		AudioSource asource = gameObject.AddComponent< AudioSource >();
		asource.playOnAwake = false;
		asource.volume = GameConfig.AUDIO_SFX_VOLUME;
		_sfxAudioSourcePool.AddObjectToPool(asource);
		return asource;
	}

	public void PlaySFX(ESFX key)
	{
		SAudioSFX sfx = GetSFX(key);
		if(sfx.key != ESFX.UNKNOWN)
		{
			AudioSource source = GetSFXAudioSource();
			if(source != null)
			{
				source.clip = sfx.audioClip;
				source.Play();
			}
		}

	}

	private AudioSource GetSFXAudioSource()
	{
		AudioSource asource;
		if(_sfxAudioSourcePool.GetUnusedObject(out asource))
		{
			return asource;
		}
		else if(_sfxAudioSourcePool.totalObjsInPool < GameConfig.AUDIO_MAX_SFX_AUDIOSOURCES) //stop the game from creating too much audiosources for sfx
		{
			asource = CreateAudioSource();
		}
		return null;
	}

	public void PlayMusic(EMUSIC key)
	{
		SAudioMusic music = GetMusic(key);
		if(music.key != EMUSIC.UNKNOWN)
		{
			StopMusic();
			_currMusic = music;
			_musicAudioSource.clip = _currMusic.audioClip;
			_musicAudioSource.Play();
		}
	}

	public void StopMusic()
	{
		if(_musicAudioSource.isPlaying)
		{
			_musicAudioSource.Stop();
		}
	}

	//performance wise this isn't the best approach, but it is more robust to changes in the ESFX enum
	private SAudioSFX GetSFX(ESFX key)
	{
		return _sfxList.Find( x => x.key == key);
	}

	//performance wise this isn't the best approach, but it is more robust to changes in the EMUSIC enum
	private SAudioMusic GetMusic(EMUSIC key)
	{
		return _musicList.Find( x => x.key == key);
	}

	[System.Serializable]
	public struct SAudioSFX
	{
		public ESFX key;
		public AudioClip audioClip;
	}	
	
	[System.Serializable]
	public struct SAudioMusic
	{
		public EMUSIC key;
		public AudioClip audioClip;
	}
}