﻿using UnityEngine;

public abstract class BaseAI {
	public CarAIPlayer car{ get; protected set; }
	protected CarsModule _carsModule;

	public BaseAI()
	{
	}

	public virtual void SetCar(CarAIPlayer car, CarsModule carsModule)
	{
		this.car = car;
		_carsModule = carsModule;
	}

	
	public abstract void UpdateAI(float dt);

	public abstract void WaypointReached();

	public virtual void OnTriggerEnterHandler(Collider other)
    {
    }

    public virtual void OnCollisionEnterHandler(Collision other)
    {
    }
}
