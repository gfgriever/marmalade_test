﻿using UnityEngine;

public class FSMAI : BaseAI
{
    private MoneyModule _moneyModule;
    private MoneyItem _closestMoney;
    
    private float _nextCollisionCheck;
    private float _timeElapsed;
    private enum EFSM
    {
        IDLE,
        WANDERING,
        GOTOMONEY,
        AVOIDCOLLISION,
        COUNT
    }
    
    private EFSM _aiState;

    public FSMAI() : base()
    {
		_moneyModule = Gameflow.GetModule< MinigameCarsModule >().moneyModule;
        _timeElapsed = 0f;
        SetNextCollisionCheck();
    }

    private void SetNextCollisionCheck()
    {
        _nextCollisionCheck = _timeElapsed + GameConfig.REACTIVE_AI_CHECK_PERIOD;
    }

    private bool ShouldDoCollisionTest()
    {
        return _timeElapsed > _nextCollisionCheck;
    }

    public override void UpdateAI(float dt)
    {
        _timeElapsed += dt;
        switch(_aiState)
        {
            case EFSM.IDLE:
                ChangeFSMState(EFSM.WANDERING);
            break;
            case EFSM.WANDERING:
                UpdateWandering();
            break;
            case EFSM.GOTOMONEY:
                UpdateGoToMoney();
                break;
            case EFSM.AVOIDCOLLISION:
                UpdateAvoidCollision();
                break;
            default:
            break;
        }
    }

     private void UpdateGoToMoney()
    {
        if(MightCollide())
        {
            ChangeFSMState(EFSM.AVOIDCOLLISION);
        }
        else
        {
            MoneyItem money = GetClosestMoney();
            if(money != _closestMoney && money != null)
            {
                _closestMoney = money;
                ChangeFSMState(EFSM.GOTOMONEY);
            }
        }
    }

    private void UpdateAvoidCollision()
    {
        if(!MightCollide()) //if the ai is not at risk of collision then go for the closest money
        {
            MoneyItem money = GetClosestMoney();
            if(money != _closestMoney && money != null)
            {
                _closestMoney = money;
                ChangeFSMState(EFSM.GOTOMONEY);
            }
        }
        else
        {
            AvoidCollision();
        }
    }

    private void UpdateWandering()
    {
        if(MightCollide())
        {
            ChangeFSMState(EFSM.AVOIDCOLLISION);
        }
        else
        {
            _closestMoney = GetClosestMoney();
            if(_closestMoney != null)
            {
                ChangeFSMState(EFSM.GOTOMONEY);
            }
        }
    }


    private void ChangeFSMState(EFSM state)
    {
        _aiState = state;
        switch(_aiState)
        {
            case EFSM.WANDERING:
                _closestMoney = null;
                SetNewRandomWaypoint();
            break;
            case EFSM.GOTOMONEY:
                car.MoveCarTo(_closestMoney.moneyObject.transform.position);
            break;
            case EFSM.AVOIDCOLLISION: //in fact we could skip this state completely, but it helps to visualize the reasoning of the AI
                AvoidCollision();
            break;
            default:
            break;
        }
    }

    private void AvoidCollision()
    {
        bool mightCollide = true;
        Vector3 waypoint = Vector3.zero;
        while(mightCollide)
        {
            //get new waypoint until one is found that is in no predictable collision
            waypoint = car.carMovement.GetRandomArenaPoint(GameConfig.ARENA_RADIUS, false);
            //check if waypoint collides
            mightCollide = RayCastTestHit(waypoint);
        }
        car.MoveCarTo(waypoint);
    }

    private bool RayCastTestHit(Vector3 waypoint)
    {
        Vector3 carPos = car.carTransform.position;
        Vector3 playerToWaypointVec = waypoint - carPos;
        float angleVariance = 30f; //GameConfig.REACTIVE_AI_RAYCAST_ANGLE_VARIANCE;
        Vector3 carUpRight = Quaternion.AngleAxis(angleVariance, Vector3.up) * playerToWaypointVec;
        Vector3 carUpLeft = Quaternion.AngleAxis(-angleVariance, Vector3.up) * playerToWaypointVec;

        bool colToWaypoint = MightCollideTraffic(carPos, playerToWaypointVec.normalized, GameConfig.REACTIVE_AI_RAYCAST_DIST );
        bool colRight = MightCollideTraffic(carPos, carUpRight, GameConfig.REACTIVE_AI_RAYCAST_DIST);
        bool colLeft = MightCollideTraffic(carPos, carUpLeft, GameConfig.REACTIVE_AI_RAYCAST_DIST);

         return colToWaypoint || colRight || colLeft;
    }

    private bool MightCollide()
    {
        //for perfomance considerations, only do the collision check periodically
        if(ShouldDoCollisionTest())
        {
            SetNextCollisionCheck();
            return RayCastTestHit(car.carMovement.nextWaypoint);
        }
        return false;
    }
    
    private bool MightCollideTraffic(Vector3 carPos, Vector3 rayDir, float rayDist)
    {
        Ray ray = new Ray(carPos, rayDir * rayDist);
        RaycastHit hit;
//        Debug.DrawRay(ray.origin, ray.direction * rayDist,Color.blue, 0.01f);
        
        if(Physics.Raycast(ray.origin, ray.direction, out hit, rayDist ))
        {
            //Debug.Break();
            return hit.collider.tag == "Traffic";
        }

        return false;
    }



   
    public override void SetCar(CarAIPlayer car, CarsModule cm)
    {
        base.SetCar(car, cm);
    }

    public override void WaypointReached()
    {
        ChangeFSMState(EFSM.WANDERING);
    }

    private void SetNewRandomWaypoint()
	{
        Vector3 nextWaypoint = car.carMovement.GetRandomArenaPoint(GameConfig.ARENA_RADIUS, false);
		
		car.MoveCarTo(nextWaypoint);
	}


	private MoneyItem GetClosestMoney()
	{
		return _moneyModule.GetClosestMoney(car.carGameObject);
	}
}
