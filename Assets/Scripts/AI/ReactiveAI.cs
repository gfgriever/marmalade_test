﻿using UnityEngine;

public class ReactiveAI : BaseAI
{

    float _nextWhiskersCheck;
    float _timeElapsed;

    public ReactiveAI() : base()
    {
    }

    public override void UpdateAI(float dt)
    {
        _timeElapsed += dt;
        //periodically do 3 raycasts to check if there are obstacles or money in the ai's path
        if(_timeElapsed >= _nextWhiskersCheck)
        {
            WhiskersCheck();
            SetNextWhiskersCheck();
        }
    }

    private void SetNextWhiskersCheck()
    {
        _nextWhiskersCheck = _timeElapsed + GameConfig.REACTIVE_AI_CHECK_PERIOD;
    }

    private void WhiskersCheck()
    {
        Vector3 carPos = car.carTransform.position;
        Vector3 carDir = car.carTransform.up * -1f;
        float angleVariance = GameConfig.REACTIVE_AI_RAYCAST_ANGLE_VARIANCE;
        Vector3 carUpRight = Quaternion.AngleAxis(angleVariance, Vector3.up) * carDir;
        Vector3 carUpLeft = Quaternion.AngleAxis(-angleVariance, Vector3.up) * carDir;

        Collider colFront = GetRayCollider(carPos, carDir);
        Collider colRight = GetRayCollider(carPos, carUpRight);
        Collider colLeft = GetRayCollider(carPos, carUpLeft);

        //AI will have priority to whatever is in front of it, if its front is clear, it will check its sides
        if(!DoActionCollider(colFront))
        {
            if(!DoActionCollider(colRight))
            {
                DoActionCollider(colLeft);
            }
        }

    }

    private bool RayCastTestHit(Vector3 waypoint)
    {
        Vector3 carPos = car.carTransform.position;
        Vector3 playerToWaypointVec = waypoint - carPos;
        float angleVariance = GameConfig.REACTIVE_AI_RAYCAST_ANGLE_VARIANCE;
        Vector3 carUpRight = Quaternion.AngleAxis(angleVariance, Vector3.up) * playerToWaypointVec;
        Vector3 carUpLeft = Quaternion.AngleAxis(-angleVariance, Vector3.up) * playerToWaypointVec;

        bool colToWaypoint = MightCollideTraffic(carPos, playerToWaypointVec.normalized, playerToWaypointVec.magnitude );
        bool colRight = MightCollideTraffic(carPos, carUpRight, playerToWaypointVec.magnitude);
        bool colLeft = MightCollideTraffic(carPos, carUpLeft, playerToWaypointVec.magnitude);

         return colToWaypoint || colRight || colLeft;
    }
    private bool MightCollideTraffic(Vector3 carPos, Vector3 rayDir, float rayDist)
    {
        Collider other = GetRayCollider(carPos, rayDir, rayDist);
        return other != null && other.tag == "Traffic";
    }

    private Collider GetRayCollider(Vector3 carPos, Vector3 rayDir)
    {
        return GetRayCollider(carPos, rayDir, GameConfig.REACTIVE_AI_RAYCAST_DIST);
    }
    private Collider GetRayCollider(Vector3 carPos, Vector3 rayDir, float rayDist)
    {
        Ray ray = new Ray(carPos, rayDir * rayDist);
        RaycastHit hit;
        //Debug.DrawRay(ray.origin, ray.direction * rayDist, Color.blue, 1f);
        if(Physics.Raycast(ray.origin, ray.direction, out hit, rayDist ))
        {
            return hit.collider;
        }

        return null;
    }

    private bool DoActionCollider(Collider col)
    {
        //nothing in the path of the raycast
        if(col == null)
        {
            return false;
        }
        else
        {   //raycast hit something
            switch(col.tag) 
            {
                case "Money":
                    PickupMoney(col);
                    return true;
                case "Traffic":
                    AvoidCollision(col);
                    return true;
                default:
                    return false;
            }
        }
    }

    private void AvoidCollision(Collider other) //simple and inaccurate way to try to avoid collision
    {
        bool mightCollide = true;
        Vector3 waypoint = Vector3.zero;
        while(mightCollide)
        {
            waypoint = car.carMovement.GetRandomArenaPoint(GameConfig.ARENA_RADIUS, false);
            mightCollide = RayCastTestHit(waypoint);
        }
        car.MoveCarTo(waypoint);
    }

    private void PickupMoney(Collider collider)
    {
        car.MoveCarTo(collider.transform.position);
    }



    public override void SetCar(CarAIPlayer car, CarsModule cm)
    {
        base.SetCar(car, cm);
        WaypointReached();
    }

    public override void WaypointReached()
    {
        SetRandomWaypoint();	
    }

    private void SetRandomWaypoint()
	{
        Vector3 nextWaypoint;
        nextWaypoint = car.carMovement.GetRandomArenaPoint(GameConfig.ARENA_RADIUS, false);
		
		car.MoveCarTo(nextWaypoint);
	}
}
