﻿using UnityEngine;

public class AnimationDetector : MonoBehaviour {

	public delegate void AnimationDelegate(string anim);
	public event AnimationDelegate OnAnimStartEvent;
	public event AnimationDelegate OnAnimEndEvent;

	public void DisableGameObject()
	{
		gameObject.SetActive(false);
	}

	public void OnAnimationStart(string anim)
	{
		if(OnAnimStartEvent != null)
		{
			OnAnimStartEvent(anim);
		}
	}

	public void OnAnimationEnd(string anim)
	{
		if(OnAnimEndEvent != null)
		{
			OnAnimEndEvent(anim);
		}
	}	
}
