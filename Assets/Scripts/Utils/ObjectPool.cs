﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool< T > {
	private List< T > _objectPool;
	public List< T > objectsInUse{ get; private set;}
	public int totalObjsInPool{get{ return _objectPool.Count + objectsInUse.Count;}}
	
	public ObjectPool() : this(new List< T > ())
	{	
	}

	public ObjectPool(List< T > objs)
	{
		_objectPool = objs;
		objectsInUse = new List< T >();
	}

	public void AddObjectsToPool(GameObject prefab, int numObjects, bool startState)
	{
        Vector3 prefabPos = prefab.transform.position;
        Quaternion prefabRot = prefab.transform.rotation;
		GameObject gobj;
		T obj; 
        for(int i = 0 ; i < numObjects; ++i)
        {
			gobj = GameObject.Instantiate(prefab, prefabPos, prefabRot);
            obj = gobj.GetComponent< T >();
			gobj.SetActive(startState);
			AddObjectToPool(obj);
        }
	}

    public void DestroyPool()
    {
		_objectPool = null;
		objectsInUse = null;
    }

    public void AddObjectsToPool(List< T > objs)
	{
		_objectPool.AddRange(objs);
	}
	public void AddObjectToPool(T obj)
	{
		_objectPool.Add(obj);
	}

	public bool GetUnusedObject(out T obj)
	{
		if(_objectPool.Count == 0)
		{
			obj = default(T);
			return false;
		}
		else
		{
			int rand = UnityEngine.Random.Range(0, _objectPool.Count);
			obj = _objectPool[rand];
			_objectPool.RemoveAt(rand);
			objectsInUse.Add(obj);
			return true;
		}
	}

    public void FreeObjects(List<T> objs)
    {
		for(int i = 0 ; i < objs.Count; ++i )
		{
			FreeObject(objs[i]);
		}
    }

    public void FreeObject(T obj)
	{
		if(objectsInUse.Contains(obj))
		{
			objectsInUse.Remove(obj);
			_objectPool.Add(obj);
		}
	}
}
