﻿using UnityEngine;

public class Singleton< T > : MonoBehaviour where T : MonoBehaviour {

    static Singleton< T > _instance;

    public static T instance { get { return _instance.GetComponent< T >(); } }

    void Awake()
    {
        if(_instance != null)
        {
            Debug.Log("Destroyed instance of " + typeof(T));
            Destroy(gameObject);
        }
        else
        {
            Debug.Log("Created instance of " + typeof(T));
            _instance = this;
            DontDestroyOnLoad(gameObject);
            LateAwake();
        }
    }

    protected virtual void LateAwake() { }

	protected virtual void OnDestroy()
	{
		
	}
}
