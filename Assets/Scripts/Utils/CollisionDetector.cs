﻿using UnityEngine;

public class CollisionDetector : MonoBehaviour {

	public delegate void OnCollisionEnterDelegate(Collision other);
	public delegate void OnTriggerEnterDelegate(Collider other);

	public event OnCollisionEnterDelegate OnCollisionEnterEvent;
	public event OnTriggerEnterDelegate OnTriggerEnterEvent;
	
	void OnCollisionEnter(Collision other)
	{
		if(OnCollisionEnterEvent != null)
		{
			OnCollisionEnterEvent(other); 
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(OnTriggerEnterEvent != null)
		{
			OnTriggerEnterEvent(other);
		}
	}
}
