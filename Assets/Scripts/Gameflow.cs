﻿using System.Collections.Generic;
using UnityEngine;

public class Gameflow : Singleton< Gameflow > {
	private List< BaseModule > _modules;

	protected override void LateAwake () {
		InitModules();
	}
	private void InitModules()
	{
		_modules = new List< BaseModule >();
		InitModule< InputModule >();
		InitModule< AudioModule >();
		InitModule< MinigameCarsModule >();
	}

	private T InitModule< T >() where T : BaseModule
	{
		T moduleVar = gameObject.GetComponentInChildren< T > ();
		_modules.Add(moduleVar);
		moduleVar.InitModule();
		return moduleVar;
	}


	public static T GetModule<T>() where T : BaseModule
    {
        BaseModule m = null;
		for(int i = 0 ; i < instance._modules.Count; ++i)
		{
			m = instance._modules[i];
            if (m.GetType() == typeof(T))
            {
                return (T)m;
            }
		}
        return default(T);
	}


	//ideally the only Unity Update method used in the application. Through here, all the modules and entities shall be updated
	void Update () {

		for(int i = 0 ; i < _modules.Count; ++i)
		{
			_modules[i].UpdateModule(Time.deltaTime);
		}
	}
	protected override void OnDestroy()
	{
		_modules = null;
	}
}
