﻿using UnityEngine;

public class MouseCarController : InputController {
    private CarPlayer _player;
    
    private bool _clickingPlayer;
    private Vector3 _lastPosition;
    public MouseCarController(CarPlayer p)
    {
        _player = p;
        _clickingPlayer = false;
        _lastPosition = Vector3.one * float.MinValue;
    }

    protected virtual bool TouchBegin()
    {
        return Input.GetMouseButtonDown(0) || Input.touchSupported && Input.GetTouch(0).phase == TouchPhase.Began;
    }

    protected virtual bool TouchDrag()
    {
        return Input.GetMouseButton(0) || Input.touchSupported && Input.GetTouch(0).phase == TouchPhase.Moved;
    }

    protected virtual bool TouchEnd()
    {
        return Input.GetMouseButtonUp(0) || Input.touchSupported && Input.GetTouch(0).phase == TouchPhase.Ended;
    }

    protected virtual Vector3 TouchPosition()
    {
        return Input.mousePosition;
    }

    public override void UpdateController(float dt)
    {
        if(TouchBegin() && ClickedPlayer())
        {
            _clickingPlayer = true;
        }
        else if(TouchDrag() && _clickingPlayer)
        {
            GoWaypoint();
        }
        else if(TouchEnd())
        {
            _clickingPlayer = false;
            GoWaypoint();
        }
    }

    private bool ClickedPlayer()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(TouchPosition());
        if(Physics.Raycast(ray, out hit))
        {
            return hit.collider.gameObject == _player.carGameObject;
        }
        return false;
    }

    public void GoWaypoint()
    {
        Vector3 currPosition =  TouchPosition();
        //no need to recalculate waypoint if the mouse hasn't moved
        if(currPosition != _lastPosition)
        {
            _lastPosition = currPosition;
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(currPosition);
            if(Physics.Raycast(ray, out hit))
            {
                _player.MoveCarTo(hit.point);
            }
        }
    }
}
