﻿using UnityEngine;

public enum EINPUT
{
	MOUSE,
	KEYBOARD1,
	COUNT
}

public abstract class InputController {

	public abstract void UpdateController(float dt);
}
