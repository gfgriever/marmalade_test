﻿using UnityEngine;

public class FingerController : MouseCarController
{
    public FingerController(CarPlayer p) : base(p)
    {
    }
	protected override bool TouchBegin()
    {
        return Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;
    }

    protected override bool TouchDrag()
    {
        return Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved;
    }

    protected override bool TouchEnd()
    {
        return Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended;
    }

    protected override Vector3 TouchPosition()
    {
        return Input.GetTouch(0).position;
    }
}
