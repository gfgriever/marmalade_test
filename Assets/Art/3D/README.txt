This is the list of models with their respective texture and small description:

car1.fbx            vehicles.png        Player1
car2.fbx            vehicles.png        Player2
car3.fbx            vehicles.png        Player3
car4.fbx            vehicles.png        Player4
money1k.fbx         SpinnerMini.png     Money Note to catch
money5k.fbx         SpinnerMini.png     Money Note to catch
money10k.fbx        SpinnerMini.png     Money Note to catch
money50k.fbx        SpinnerMini.png     Money Note to catch
pickup.fbx          vehicles.png        NPC cars that players need to avoid
spinnerstatic.fbx   SpinnerMini.png     The playground